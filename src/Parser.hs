module Parser where

import           Control.Applicative            ( Alternative((<|>), empty, many, some) )
import           Control.Monad                  ( void )
import           Data.Char                      ( isAlpha
                                                , isAlphaNum
                                                , isDigit
                                                , isLower
                                                , isSpace
                                                , isUpper
                                                )

newtype Parser a = Parser { runParser :: String -> [(a, String)] }

instance Functor Parser where
    fmap f p = Parser $ \input -> g <$> (runParser p $ input) where g (a, s) = (f a, s)

instance Applicative Parser where
    pure v = Parser $ \s -> [(v, s)]
    (Parser fab) <*> (Parser ga) = Parser $ \input -> [ (f a, s) | (f, out) <- fab input, (a, s) <- ga out ]

instance Monad Parser where
    return = pure
    (Parser ga) >>= fp = Parser $ \input -> [ (b, s) | (a, out) <- (ga input), (b, s) <- runParser (fp a) out ]

instance Alternative Parser where
    empty = Parser (const [])
    (Parser p1) <|> (Parser p2) = Parser $ \input ->
        let r = p1 input
        in  case r of
                [] -> p2 input
                _  -> r

-- Parse single character
item :: Parser Char
item = Parser f
  where
    f []       = []
    f (x : xs) = [(x, xs)]

-- Parse single character that satisfies predicate
satisfies :: (Char -> Bool) -> Parser Char
satisfies p = do
    x <- item
    if p x then return x else empty

digit :: Parser Char
digit = satisfies isDigit

lower :: Parser Char
lower = satisfies isLower

upper :: Parser Char
upper = satisfies isUpper

alphabetic :: Parser Char
alphabetic = satisfies isAlpha

alphanum :: Parser Char
alphanum = satisfies isAlphaNum

char :: Char -> Parser Char
char x = satisfies (== x)

string :: String -> Parser String
string []       = return []
string (x : xs) = do
    void $ char x
    void $ string xs
    return (x : xs)

ident :: Parser String
ident = do
    x  <- lower
    xs <- many alphanum
    return (x : xs)

nat :: Parser Int
nat = do
    xs <- some digit
    return (read xs)

space :: Parser ()
space = many (satisfies isSpace) >> return ()

int :: Parser Int
int = let negative = char '-' >> (fmap negate nat) in negative <|> nat

--
-- Utilities defined in terms of token
--

-- Ignore any space before and after target
token :: Parser a -> Parser a
token p = do
    space
    v <- p
    space
    return v

identifier :: Parser String
identifier = token ident

natural :: Parser Int
natural = token nat

integer :: Parser Int
integer = token int

positiveReal :: Parser Double
positiveReal = intPointDecimal <|> pointDecimal <|> intPoint <|> fromNatural
  where
    fromNatural = fromIntegral <$> natural
    --
    intPoint    = do
        r <- fromIntegral <$> natural
        void $ char '.'
        return r
    --
    pointDecimal = do
        void $ char '.'
        d <- natural
        let power = 10 ** (fromIntegral $ numDigits d)
        if power > 0 then return $ fromIntegral d / power else return 0
    --
    intPointDecimal = do
        r <- fromIntegral <$> natural
        void $ char '.'
        d <- natural
        let power = 10 ** (fromIntegral $ numDigits d)
        if power > 0 then return $ r + (fromIntegral d / power) else return r
    --
    numDigits :: Int -> Int
    numDigits n = round (logBase (10 :: Double) (fromIntegral n)) + 1

real :: Parser Double
real = let negative = char '-' >> (fmap negate positiveReal) in token negative <|> token positiveReal

symbol :: String -> Parser String
symbol = token . string
