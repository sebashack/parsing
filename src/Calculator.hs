{-# LANGUAGE GADTs #-}

module Calculator
    ( parseExpr
    , eval
    ) where

import           Control.Applicative            ( Alternative((<|>)) )
import           Parser


data BinOp = Plus | Minus | Mult | Div deriving Show
data UnaryOp = Cos | Sin deriving Show
data NAryOp = Summation | Product deriving Show

data Expr where
  Constant ::Double -> Expr
  Bin ::BinOp -> Expr -> Expr -> Expr
  Unary::UnaryOp -> Expr -> Expr
  NAry ::NAryOp -> [Expr] -> Expr
  deriving Show

parseExpr :: String -> Maybe Expr
parseExpr input = case runParser expr $ input of
    []           -> Nothing
    ((v, _) : _) -> Just v

eval :: Expr -> Double
eval (Constant k) = k
eval (NAry _ [e]) = eval e
eval (NAry op exps) =
    let f = case op of
            Summation -> (+)
            Product   -> (*)
    in  foldr (\e v -> f v (eval e)) (eval $ head exps) (tail exps)
eval (Bin op e1 e2) =
    let leftExp  = eval e1
        rightExp = eval e2
    in  case op of
            Plus  -> leftExp + rightExp
            Minus -> leftExp - rightExp
            Mult  -> leftExp * rightExp
            Div   -> leftExp / rightExp
eval (Unary op e) =
    let degToRads degs = degs * (pi / 180)
        e' = degToRads $ eval e
    in  case op of
            Cos -> cos e'
            Sin -> sin e'

-- Helpers
expr :: Parser Expr
expr = token (binaryTerm >>= rest)
  where
    rest :: Expr -> Parser Expr
    rest e1 =
        let nextOperand = do
                op <- plusOrMinusOp
                e2 <- binaryTerm
                rest (Bin op e1 e2)
        in  nextOperand <|> return e1

binaryTerm :: Parser Expr
binaryTerm = token (factor >>= rest)
  where
    rest :: Expr -> Parser Expr
    rest e1 =
        let nextOperand = do
                op <- multOrDivOp
                e2 <- factor
                rest (Bin op e1 e2)
        in  nextOperand <|> return e1

factor :: Parser Expr
factor = token (constant <|> paren expr <|> unaryTerm <|> naryTerm)

unaryTerm :: Parser Expr
unaryTerm = do
    op <- trigOp
    leftParen
    e <- expr
    rightParen
    return $ Unary op e

naryTerm :: Parser Expr
naryTerm = do
    op <- naryOp
    leftParen
    es <- exps []
    return (NAry op es)
  where
    exps :: [Expr] -> Parser [Expr]
    exps accum = do
        e <- expr
        let accum' = accum <> [e]
        (symbol "," >> exps accum') <|> (rightParen >> return accum')

constant :: Parser Expr
constant = Constant <$> real

plusOrMinusOp :: Parser BinOp
plusOrMinusOp = (symbol "+" >> pure Plus) <|> (symbol "-" >> pure Minus)

multOrDivOp :: Parser BinOp
multOrDivOp = (symbol "*" >> pure Mult) <|> (symbol "/" >> pure Div)

trigOp :: Parser UnaryOp
trigOp = (symbol "cos" >> pure Cos) <|> (symbol "sin" >> pure Sin)

naryOp :: Parser NAryOp
naryOp = (symbol "summation" >> pure Summation) <|> (symbol "product" >> pure Product)

paren :: Parser Expr -> Parser Expr
paren p = do
    leftParen
    e <- p
    rightParen
    return e

leftParen :: Parser ()
leftParen = (symbol "(" >> pure ())

rightParen :: Parser ()
rightParen = (symbol ")" >> pure ())
