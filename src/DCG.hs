{-# LANGUAGE GADTs #-}
{-# LANGUAGE TupleSections #-}

module DCG where


import           Control.Applicative            ( Alternative((<|>), empty) )
import           Control.Monad                  ( foldM )
import           Parser


type Atom = String
type Phrase = Expr

data Expr where
  Sentence ::Phrase -> Phrase -> Expr
  NounPhrase ::Expr -> Expr -> Expr
  VerbPhrase ::Expr -> Phrase -> Expr
  Det ::Atom -> Expr
  Noun ::Atom -> Expr
  Verb ::Atom -> Expr
  deriving Show

data Mode = Selective | Generative

eval :: Expr -> String
eval (Sentence   p1 p2) = eval p1 <> " " <> eval p2
eval (NounPhrase e1 e2) = eval e1 <> " " <> eval e2
eval (VerbPhrase e  p ) = eval e <> " " <> eval p
eval (Det  d          ) = d
eval (Noun n          ) = n
eval (Verb v          ) = v

isMember :: String -> Maybe Expr
isMember input = case runParser (sentence Selective) $ input of
    []           -> Nothing
    ((v, _) : _) -> Just v

allMembers :: [Expr]
allMembers = fst <$> runParser (sentence Generative) "#X"

sentence :: Mode -> Parser Expr
sentence mode = do
    np <- nounPhrase
    vp <- verbPhrase
    return $ Sentence np vp
  where
    verbPhrase :: Parser Phrase
    verbPhrase = do
        v  <- verb mode
        np <- nounPhrase
        return $ VerbPhrase v np
    --
    nounPhrase :: Parser Phrase
    nounPhrase = do
        d <- det mode
        n <- noun mode
        return $ NounPhrase d n

atom :: Mode -> (String -> Expr) -> [String] -> Parser Expr
atom mode f atoms = case mode of
    Selective  -> selectiveParser
    Generative -> generativeParser
  where
    selectiveParser  = foldr (\s p -> (f <$> symbol s) <|> p) empty atoms
    --
    generativeParser = Parser $ \_ -> (, "#X") . f <$> atoms

det :: Mode -> Parser Expr
det mode = atom mode Det ["the", "a"]

noun :: Mode -> Parser Expr
noun mode = atom mode Noun ["cat", "bat"]

verb :: Mode -> Parser Expr
verb mode = atom mode Verb ["eats", "beats"]
