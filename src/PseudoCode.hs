{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module PseudoCode where

import           Control.Applicative            ( Alternative((<|>), empty)
                                                , many
                                                )
import           Control.Monad                  ( void )
import           Control.Monad.State.Class      ( MonadState )
import           Control.Monad.State.Lazy       ( StateT(..)
                                                , modify
                                                , runStateT
                                                , state
                                                )
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Parser


type Assignments = Map String (Type, (Maybe Value))

newtype ParserS a = ParserS { unParserS :: StateT Assignments Parser a }
                 deriving ( Functor
                          , Applicative
                          , Monad
                          , MonadState Assignments
                          )

instance Alternative ParserS where
    empty = liftP empty
    p1 <|> p2 = ParserS $ StateT $ \s -> do
        let f1 = (runStateT . unParserS) p1 $ s
            f2 = (runStateT . unParserS) p2 $ s
        Parser $ \input ->
            let r = runParser f1 $ input
            in  case r of
                    [] -> runParser f2 $ input
                    _  -> r

data Reserved = Start | End deriving Show
data IOOp = Print | Write deriving Show
data Type = FLOAT | BOOL deriving Show
data Value = VF Double | VB Bool deriving Show

data Expr where
  Program ::Reserved -> [Expr] -> Reserved -> Expr
  Declaration ::Type -> String -> Expr
  Assignment ::String -> Value -> Expr
  Constant ::Value -> Expr
  Var ::String -> Expr
  IO' ::IOOp -> Either String Value -> Expr
  deriving Show

parseProgram :: String -> Maybe (Expr, Assignments)
parseProgram input =
    let result = runParser (runStateT (unParserS program) $ M.empty) input
    in  case result of
            []           -> Nothing
            ((v, _) : _) -> Just v

program :: ParserS Expr
program = do
    start <- liftP progStart
    es    <- exprs []
    end   <- liftP progEnd
    return $ Program start es end
  where
    exprs :: [Expr] -> ParserS [Expr]
    exprs accum = do
        e <- command
        let accum' = accum <> [e]
        exprs accum' <|> return accum'

command :: ParserS Expr
command = decl <|> assignment <|> progPrint

progStart :: Parser Reserved
progStart = symbol "start" >> pure Start

progEnd :: Parser Reserved
progEnd = symbol "end" >> pure End

progPrint :: ParserS Expr
progPrint = do
    liftP $ symbol "print"
    liftP $ leftParen
    expr <- (Left <$> liftP identifier) <|> (Right <$> liftP value)
    liftP rightParen
    liftP spaceChars
    liftP semicolon
    return $ IO' Print expr

typ :: Parser Type
typ = (symbol "float" >> pure FLOAT) <|> (symbol "bool" >> pure BOOL)

decl :: ParserS Expr
decl = do
    t <- liftP typ
    liftP spaceChars
    idt <- liftP identifier
    liftP spaceChars
    liftP semicolon
    let f as = if isNonDupDecl idt as
            then M.insert idt (t, Nothing) as
            else error "Error: Duplicate variable declaration."
    modify f
    return $ Declaration t idt

assignment :: ParserS Expr
assignment = do
    idt <- liftP identifier
    liftP spaceChars
    liftP $ void $ symbol "="
    liftP spaceChars
    val <- liftP value
    liftP spaceChars
    liftP semicolon
    modify (assign idt val)
    return $ Assignment idt val

value :: Parser Value
value = (real >>= (pure . VF)) <|> bool
  where
    bool :: Parser Value
    bool = (symbol "true" >> pure (VB True)) <|> (symbol "false" >> pure (VB False))

semicolon :: Parser ()
semicolon = symbol ";" >> pure ()

leftParen :: Parser ()
leftParen = (symbol "(" >> pure ())

rightParen :: Parser ()
rightParen = (symbol ")" >> pure ())

spaceChars :: Parser ()
spaceChars = many (char ' ') >> pure ()

assign :: String -> Value -> Assignments -> Assignments
assign idt val as = case M.lookup idt as of
    Nothing     -> error $ "Error: " <> (show idt) <> " " <> " is not declared."
    Just (t, _) -> if isValidTypeVal t val
        then M.insert idt (t, Just val) as
        else error $ "Error: " <> "type is " <> show t <> " but" <> " value is " <> show val

isValidTypeVal :: Type -> Value -> Bool
isValidTypeVal t v = case (t, v) of
    (BOOL , VB _) -> True
    (FLOAT, VF _) -> True
    _             -> False

isNonDupDecl :: String -> Assignments -> Bool
isNonDupDecl idt as = case M.lookup idt as of
    Nothing -> True
    _       -> False

liftP :: Parser a -> ParserS a
liftP parser = ParserS $ StateT $ \s -> (\a -> (a, s)) <$> parser
