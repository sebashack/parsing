module Main where

import           DCG

main :: IO ()
main = do
    print $ eval <$> allMembers
    let maybeExpr = isMember "the cat eats the bat"
    case maybeExpr of
        Nothing   -> error "Could not parse expression"
        Just expr -> print $ eval expr
